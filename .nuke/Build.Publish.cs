using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tools.DotNet;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace AtomicMassa.NUKE;

/// <summary>
/// This is the main build file for the project.
/// This partial is responsible for the publish process.
/// </summary>
sealed partial class Build
{
    [Parameter(
        "Runtime identifier for the build (e.g., win-x64, linux-x64, osx-x64) (default: linux-x64)"
    )]
    readonly string runtimeIdentifier = "linux-x64";

    [Parameter("publish-directory (default: ./publish/{runtimeIdentifier})")]
    readonly AbsolutePath publishDirectory;
    AbsolutePath PublishDirectory =>
        publishDirectory ?? RootDirectory / "publish" / runtimeIdentifier;

    [Parameter("publish-self-contained (default: true)")]
    readonly bool publishSelfContained = true;

    [Parameter("publish-single-file (default: true)")]
    readonly bool publishSingleFile = true;

    [Parameter("publish-trimmed (default: false)")]
    readonly bool publishTrimmed;

    readonly string[] paths = ["Assets, Plataform, Plataforms"];

    public Target Publish => td =>
        td
            .DependsOn(Restore)
            .Executes(() =>
            {
                foreach (var project in Solution.AllProjects)
                {
                    // Skip test/nuke projects
                    var isPackable = project.GetProperty("IsPackable");
                    if (isPackable is not null && isPackable == "false")
                    {
                        continue;
                    }
                    var isExecutable = project.GetOutputType() == "Exe" || project.GetOutputType() == "WinExe";

                    _ = DotNetPublish(
                        settings =>
                            settings
                                .SetProject(project)
                                .SetNoLogo(true)
                                .SetConfiguration(Configuration)
                                .SetOutput(PublishDirectory / project.Name)
                                .SetRuntime(runtimeIdentifier)
                                .SetSelfContained(publishSelfContained && isExecutable) // Apply conditionally
                                .SetPublishSingleFile(publishSingleFile && isExecutable) // Apply conditionally
                                .SetPublishTrimmed(publishTrimmed && isExecutable) // Apply conditionally
                                .SetAuthors("Bruno Massa")
                                .SetVersion(CurrentVersion)
                                .SetAssemblyVersion(CurrentVersion)
                                .SetInformationalVersion(CurrentVersion)
                    );

                    // Copy folders to publish directory if it exists within the project directory.
                    foreach (var path in paths)
                    {
                        CreateFolder(project.Directory, path);
                    }
                }
            });

    void CreateFolder(AbsolutePath absolutePath, string path)
    {
        var assetDirectory = absolutePath / path;
        if (assetDirectory.DirectoryExists())
        {
            var publishAssetDirectory = PublishDirectory / "Assets";
            CopyDirectoryRecursively(assetDirectory, publishAssetDirectory);
        }
    }
}