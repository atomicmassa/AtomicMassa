using System;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tools.Coverlet;
using Nuke.Common.Tools.ReportGenerator;
using Serilog;
using static Nuke.Common.Tools.Coverlet.CoverletTasks;
using static Nuke.Common.Tools.ReportGenerator.ReportGeneratorTasks;

namespace AtomicMassa.NUKE;

/// <summary>
/// This is the main build file for the project.
/// This partial is responsible for the build process.
/// </summary>
sealed partial class Build
{
    static AbsolutePath TestProjectDirectory => RootDirectory / "AtomicMassa.Tests";
    AbsolutePath TestDllDirectory => TestProjectDirectory / "bin" / Configuration / "net8.0";
    AbsolutePath TestAssembly => TestDllDirectory / "test.dll";
    static AbsolutePath CoverageDirectory => RootDirectory / "coverage-results";
    static AbsolutePath CoverageResultDirectory => CoverageDirectory / "coverage";
    static AbsolutePath CoverageResultFile => CoverageResultDirectory / "coverage.xml";
    static AbsolutePath CoverageReportDirectory => CoverageDirectory / "report";
    static AbsolutePath CoverageReportSummaryDirectory => CoverageReportDirectory / "Summary.txt";

    Target Test => td =>
        td
            .DependsOn(Restore, Compile)
            .Executes(() =>
            {
                _ = CoverageResultDirectory.CreateDirectory();
                _ = Coverlet(
                    s =>
                        s.SetTarget("dotnet")
                            .SetTargetArgs($"test --no-build --no-restore -c {Configuration}")
                            .SetAssembly(TestAssembly)
                            // .SetThreshold(75) // On day, one day...
                            .SetOutput(CoverageResultFile)
                            .SetFormat(CoverletOutputFormat.cobertura)
                );
            });

    public Target TestReport => td =>
        td
            .DependsOn(Test)
            .Executes(() =>
            {
                _ = CoverageReportDirectory.CreateDirectory();
                _ = ReportGenerator(
                    s =>
                        s.SetTargetDirectory(CoverageReportDirectory)
                            .SetReportTypes(
                                [ReportTypes.Html, ReportTypes.TextSummary]
                            )
                            .SetReports(CoverageResultFile)
                );
                var summaryText = CoverageReportSummaryDirectory.ReadAllLines();
                Log.Information(string.Join(Environment.NewLine, summaryText));
            });
}