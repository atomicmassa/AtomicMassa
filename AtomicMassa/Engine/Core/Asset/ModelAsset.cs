using AtomicMassa.Engine.Core.Asset.Database;

namespace AtomicMassa.Engine.Core.Asset;

/// <summary>
/// Represents a model asset, which can be used to load a 3D model from a file.
/// </summary>
public class ModelAsset : Asset
{
    /// <summary>
    /// Gets the content of this model asset by loading a 3D model from the specified Vulkan context.
    /// </summary>
    /// <param name="vulkan">The Vulkan context used for loading the model.</param>
    /// <returns>A loaded 3D model or <c>null</c> if the asset could not be found or loaded.</returns>
    public Model? GetContent(Vulkan vulkan)
    {
        if (AssetDatabase.Instance.Assets.TryGetValue(Id, out var asset))
        {
            return ModelUtils.LoadModelFromFile(vulkan, asset.FilePath);
        }
        return null;
    }
}
