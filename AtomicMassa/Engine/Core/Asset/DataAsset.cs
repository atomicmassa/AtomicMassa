namespace AtomicMassa.Engine.Core.Asset;

/// <summary>
/// Represents a data asset within the AtomicMassa engine.
/// </summary>
public class DataAsset : Asset
{
    /// <summary>
    /// Retrieves the content associated with the specified asset path.
    /// </summary>
    /// <param name="projectPath">The path of the project where the asset is in.</param>
    /// <returns>An instance of <see cref="AmObject"/> if the asset content is found; otherwise, null.</returns>
    public AmObject? GetContent(string projectPath)
    {
        return LoadContent(Path.Combine(projectPath, RelativePath));
    }

    /// <summary>
    /// Loads the content from the specified absolute path.
    /// </summary>
    /// <param name="absolutePath">The absolute path to load content from.</param>
    /// <returns>An instance of <see cref="AmObject"/> if the content is successfully loaded; otherwise, an exception is thrown.</returns>
    /// <exception cref="Exception">Thrown when the content fails to load from the given path.</exception>
    public static AmObject? LoadContent(string absolutePath)
    {
        if (File.Exists(absolutePath))
        {
            return Load<AmObject>(absolutePath);
        }
        throw new FileNotFoundException($"DataAsset load failed {absolutePath}");
    }
}
