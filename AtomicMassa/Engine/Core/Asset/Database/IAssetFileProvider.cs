namespace AtomicMassa.Engine.Core.Asset.Database;

/// <summary>
/// Represents an interface for providing access to asset files as streams.
/// </summary>
public interface IAssetFileProvider
{
    /// <summary>
    /// Gets a <see cref="Stream"/> representing the asset file's content.
    /// </summary>
    /// <returns>A <see cref="Stream"/> containing the asset file's content.</returns>
    Stream GetAssetStream();
}
