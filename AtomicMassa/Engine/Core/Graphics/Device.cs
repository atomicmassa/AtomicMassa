namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Represents a Vulkan device and its associated resources for graphics and rendering operations.
/// This class encapsulates functionality for creating and managing a Vulkan device, interfacing with surfaces,
/// and providing access to Vulkan instance-related functionality.
/// </summary>
/// <remarks>
/// The Vulkan device is a core component for graphics rendering and computation in Vulkan-based applications.
/// It handles resource management, command buffer creation, and other critical aspects of rendering.
/// The class also provides access to the Vulkan instance, which manages global state for Vulkan.
/// </remarks>
public unsafe class Device : IDisposable
{
    /// <summary>
    /// Gets the Vulkan instance associated with this device.
    /// </summary>
    public Instance Instance => instance;

    /// <summary>
    /// Gets the Vulkan device.
    /// </summary>
    public Silk.NET.Vulkan.Device VkDevice => device;

    /// <summary>
    /// Gets the surface associated with this device.
    /// </summary>
    public SurfaceKHR Surface => surface;

    /// <summary>
    /// Gets the physical device associated with this device.
    /// </summary>
    public PhysicalDevice VkPhysicalDevice => physicalDevice;

    /// <summary>
    /// Gets the name of the Vulkan device.
    /// </summary>
    public string DeviceName => deviceName;

    /// <summary>
    /// Gets the MSAA (Multi-Sample Anti-Aliasing) sample count.
    /// </summary>
    public SampleCountFlags MsaaSamples => msaaSamples;

    /// <summary>
    /// Gets the index of the graphics family queue.
    /// </summary>
    public uint GraphicsFamilyIndex => graphicsFamilyIndex;

    /// <summary>
    /// Gets the graphics queue associated with this device.
    /// </summary>
    public Queue GraphicsQueue => graphicsQueue;

    /// <summary>
    /// Gets the present queue associated with this device.
    /// </summary>
    public Queue PresentQueue => presentQueue;

    /// <summary>
    /// Gets the command pool associated with this device.
    /// </summary>
    public CommandPool CommandPool => commandPool;

    /// <summary>
    /// Gets the queue families found for this device.
    /// </summary>
    public QueueFamilyIndices FindQueueFamilies => FindQueueFamiliesInternal(physicalDevice);

    /// <summary>
    /// Gets the swap chain support details for this device.
    /// </summary>
    public SwapChainSupportDetails QuerySwapChainSupport =>
        QuerySwapChainSupportInternal(physicalDevice);

    private readonly Vk vk;
    private readonly IView window;

    private ExtDebugUtils debugUtils = null!;
    private DebugUtilsMessengerEXT debugMessenger;

    private readonly bool enableValidationLayers = false;
    private readonly string[] validationLayers = ["VK_LAYER_KHRONOS_validation"];

    private readonly string[] deviceExtensions =
    [
        KhrSwapchain.ExtensionName,
        KhrSynchronization2.ExtensionName,
        "VK_EXT_mesh_shader",
        //"VK_KHR_spirv_1_4",
        //"VK_KHR_shader_float_controls",
    ];

    private Instance instance;
    private KhrSurface khrSurface = null!;
    private SurfaceKHR surface;
    private PhysicalDevice physicalDevice;
    private string deviceName = "unknown";
    private SampleCountFlags msaaSamples = SampleCountFlags.Count1Bit;
    private Silk.NET.Vulkan.Device device;
    private uint graphicsFamilyIndex;
    private Queue graphicsQueue;
    private Queue presentQueue;
    private CommandPool commandPool;

    /// <summary>
    /// Initializes a new instance of the <see cref="Device"/> class.
    /// </summary>
    /// <param name="vk">The Vulkan instance.</param>
    /// <param name="window">The window view.</param>
    public Device(Vk vk, IView window)
    {
        this.vk = vk;
        this.window = window;
        CreateInstance();
        SetupDebugMessenger();
        CreateSurface();
        CreateLogicalDevice();
        CreateCommandPool();

        Log.Logger.Lap("startup", "got device");
    }

    /// <summary>
    /// Gets the properties of the physical device.
    /// </summary>
    /// <returns>The properties of the physical device.</returns>
    public PhysicalDeviceProperties GetProperties()
    {
        vk.GetPhysicalDeviceProperties(physicalDevice, out var properties);
        return properties;
    }

    /// <summary>
    /// Copies data from one buffer to another.
    /// </summary>
    /// <param name="srcBuffer">The source buffer.</param>
    /// <param name="dstBuffer">The destination buffer.</param>
    /// <param name="size">The size of the data to copy.</param>
    public void CopyBuffer(
        Silk.NET.Vulkan.Buffer srcBuffer,
        Silk.NET.Vulkan.Buffer dstBuffer,
        ulong size
    )
    {
        var commandBuffer = BeginSingleTimeCommands();

        BufferCopy copyRegion = new() { Size = size, };

        vk.CmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, copyRegion);

        EndSingleTimeCommands(commandBuffer);
    }

    /// <summary>
    /// Creates a buffer with the specified size, usage, and memory properties.
    /// </summary>
    /// <param name="size">The size of the buffer.</param>
    /// <param name="usage">The buffer usage flags.</param>
    /// <param name="properties">The memory properties.</param>
    /// <param name="buffer">The created buffer.</param>
    /// <param name="bufferMemory">The memory for the buffer.</param>
    public void CreateBuffer(
        ulong size,
        BufferUsageFlags usage,
        MemoryPropertyFlags properties,
        ref Silk.NET.Vulkan.Buffer buffer,
        ref DeviceMemory bufferMemory
    )
    {
        BufferCreateInfo bufferInfo =
            new()
            {
                SType = StructureType.BufferCreateInfo,
                Size = size,
                Usage = usage,
                SharingMode = SharingMode.Exclusive,
            };

        fixed (Silk.NET.Vulkan.Buffer* bufferPtr = &buffer)
        {
            var resultVulkan = vk.CreateBuffer(device, bufferInfo, null, bufferPtr);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create vertex buffer: {0}", resultVulkan);
            }
        }

        MemoryRequirements memRequirements = new();
        vk.GetBufferMemoryRequirements(device, buffer, out memRequirements);

        MemoryAllocateInfo allocateInfo =
            new()
            {
                SType = StructureType.MemoryAllocateInfo,
                AllocationSize = memRequirements.Size,
                MemoryTypeIndex = FindMemoryType(memRequirements.MemoryTypeBits, properties),
            };

        fixed (DeviceMemory* bufferMemoryPtr = &bufferMemory)
        {
            var resultVulkan = vk.AllocateMemory(device, allocateInfo, null, bufferMemoryPtr);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to allocate vertex buffer memory: {0}", resultVulkan);
            }
        }

        _ = vk.BindBufferMemory(device, buffer, bufferMemory, 0);
    }

    /// <summary>
    /// Finds the depth format supported by the device.
    /// </summary>
    /// <returns>The supported depth format.</returns>
    public Format FindDepthFormat()
    {
        return FindSupportedFormat(
            new[] { Format.D32Sfloat, Format.D32SfloatS8Uint, Format.D24UnormS8Uint },
            ImageTiling.Optimal,
            FormatFeatureFlags.DepthStencilAttachmentBit
        );
    }

    /// <summary>
    /// Finds a suitable memory type for the specified type filter and memory properties.
    /// </summary>
    /// <param name="typeFilter">The type filter.</param>
    /// <param name="properties">The memory properties.</param>
    /// <returns>The memory type index.</returns>
    public uint FindMemoryType(uint typeFilter, MemoryPropertyFlags properties)
    {
        vk.GetPhysicalDeviceMemoryProperties(physicalDevice, out var memProperties);

        for (var i = 0; i < memProperties.MemoryTypeCount; i++)
        {
            if (
                (typeFilter & (1 << i)) != 0
                && (memProperties.MemoryTypes[i].PropertyFlags & properties) == properties
            )
            {
                return (uint)i;
            }
        }

        throw new VulkanException("failed to find suitable memory type!");
    }

    private void CreateInstance()
    {
        if (enableValidationLayers && !CheckValidationLayerSupport())
        {
            throw new VulkanException("validation layers requested, but not available!");
        }

        ApplicationInfo appInfo =
            new()
            {
                SType = StructureType.ApplicationInfo,
                PApplicationName = (byte*)Marshal.StringToHGlobalAnsi("Hello Triangle"),
                ApplicationVersion = new Version32(1, 0, 0),
                PEngineName = (byte*)Marshal.StringToHGlobalAnsi("No Engine"),
                EngineVersion = new Version32(1, 0, 0),
                ApiVersion = Vk.Version12
            };

        InstanceCreateInfo createInfo =
            new() { SType = StructureType.InstanceCreateInfo, PApplicationInfo = &appInfo };

        var extensions = GetRequiredExtensions();
        createInfo.EnabledExtensionCount = (uint)extensions.Length;
        createInfo.PpEnabledExtensionNames = (byte**)SilkMarshal.StringArrayToPtr(extensions);

        if (enableValidationLayers)
        {
            createInfo.EnabledLayerCount = (uint)validationLayers.Length;
            createInfo.PpEnabledLayerNames = (byte**)SilkMarshal.StringArrayToPtr(validationLayers);

            DebugUtilsMessengerCreateInfoEXT debugCreateInfo = new();
            PopulateDebugMessengerCreateInfo(ref debugCreateInfo);
            createInfo.PNext = &debugCreateInfo;
        }
        else
        {
            createInfo.EnabledLayerCount = 0;
            createInfo.PNext = null;
        }

        var resultVulkan = vk.CreateInstance(createInfo, null, out instance);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create instance: {0}", resultVulkan);
        }

        Marshal.FreeHGlobal((IntPtr)appInfo.PApplicationName);
        Marshal.FreeHGlobal((IntPtr)appInfo.PEngineName);
        _ = SilkMarshal.Free((nint)createInfo.PpEnabledExtensionNames);

        if (enableValidationLayers)
        {
            _ = SilkMarshal.Free((nint)createInfo.PpEnabledLayerNames);
        }
    }

    private void CreateSurface()
    {
        if (!vk.TryGetInstanceExtension<KhrSurface>(instance, out khrSurface))
        {
            throw new NotSupportedException("KHR_surface extension not found.");
        }

        if (window.VkSurface is null)
        {
            throw new VulkanException("window.VkSurface is null and shouldn't be!");
        }

        surface = window.VkSurface
            .Create<AllocationCallbacks>(instance.ToHandle(), null)
            .ToSurface();
    }

    private unsafe void CreateLogicalDevice()
    {
        uint deviceCount = 0;
        _ = vk.EnumeratePhysicalDevices(instance, ref deviceCount, null);

        if (deviceCount == 0)
        {
            throw new VulkanException("failed to find GPUs with Vulkan support!");
        }

        var devices = new PhysicalDevice[deviceCount];
        fixed (PhysicalDevice* devicesPtr = devices)
        {
            _ = vk.EnumeratePhysicalDevices(instance, ref deviceCount, devicesPtr);
        }

        var found = false;
        foreach (var deviceToTry in devices)
        {
            if (IsDeviceSuitable(deviceToTry))
            {
                try
                {
                    device = CreateLogicalDevice(deviceToTry);
                    physicalDevice = deviceToTry;
                    msaaSamples = GetMaxUsableSampleCount();
                    found = true;
                    break;
                }
                catch
                {
                }
            }
        }

        if (!found)
        {
            throw new VulkanException("Vulkan: failed to find a suitable GPU");
        }

        vk.GetPhysicalDeviceProperties(physicalDevice, out var properties);
        deviceName = GetStringFromBytePointer(properties.DeviceName, 50).Trim();
        Log.Logger.Lap("device", $"using {deviceName}");
    }

    private static string GetStringFromBytePointer(byte* pointer, int length)
    {
        // Create a span from the byte pointer and decode the string
        var span = new Span<byte>(pointer, length);
        return Encoding.UTF8.GetString(span);
    }

    private Silk.NET.Vulkan.Device CreateLogicalDevice(PhysicalDevice physicalDevice)
    {
        var indices = FindQueueFamiliesInternal(physicalDevice);

        var uniqueQueueFamilies = new[]
        {
            indices.GraphicsFamily!.Value,
            indices.PresentFamily!.Value
        }.Distinct().ToArray();

        graphicsFamilyIndex = indices.GraphicsFamily.Value;

        using var mem = GlobalMemory.Allocate(
            uniqueQueueFamilies.Length * sizeof(DeviceQueueCreateInfo)
        );
        var queueCreateInfos = (DeviceQueueCreateInfo*)
            Unsafe.AsPointer(ref mem.GetPinnableReference());

        var queuePriority = 1.0f;
        for (var i = 0; i < uniqueQueueFamilies.Length; i++)
        {
            queueCreateInfos[i] = new()
            {
                SType = StructureType.DeviceQueueCreateInfo,
                QueueFamilyIndex = uniqueQueueFamilies[i],
                QueueCount = 1,
                PQueuePriorities = &queuePriority
            };
        }

        // PhysicalDeviceFeatures deviceFeatures = new() { SamplerAnisotropy = true, };

        PhysicalDeviceMeshShaderFeaturesNV meshShaderFeaturesExt =
            new()
            {
                SType = StructureType.PhysicalDeviceMeshShaderFeaturesExt,
                MeshShader = Vk.True,
                TaskShader = Vk.True,
            };

        // Enable Synchronization 2 to eliminate a validation layer error, thanks gpt4!
        var sync2Features = new PhysicalDeviceSynchronization2FeaturesKHR
        {
            SType = StructureType.PhysicalDeviceSynchronization2FeaturesKhr,
            Synchronization2 = Vk.True
        };

        var features2 = new PhysicalDeviceFeatures2
        {
            SType = StructureType.PhysicalDeviceFeatures2,
            PNext = &sync2Features
        };
        meshShaderFeaturesExt.PNext = &sync2Features;

        features2.Features = new PhysicalDeviceFeatures { SamplerAnisotropy = Vk.False };

        var createInfo = new DeviceCreateInfo
        {
            SType = StructureType.DeviceCreateInfo,
            QueueCreateInfoCount = (uint)uniqueQueueFamilies.Length,
            PQueueCreateInfos = queueCreateInfos,
            PNext = &features2,
            EnabledExtensionCount = (uint)deviceExtensions.Length,
            PpEnabledExtensionNames = (byte**)SilkMarshal.StringArrayToPtr(deviceExtensions)
        };

        if (enableValidationLayers)
        {
            createInfo.EnabledLayerCount = (uint)validationLayers.Length;
            createInfo.PpEnabledLayerNames = (byte**)SilkMarshal.StringArrayToPtr(validationLayers);
        }
        else
        {
            createInfo.EnabledLayerCount = 0;
        }

        var resultVulkan = vk.CreateDevice(physicalDevice, in createInfo, null, out var logicalDevice);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create logical device: {0}", resultVulkan);
        }

        vk.GetDeviceQueue(logicalDevice, indices.GraphicsFamily!.Value, 0, out graphicsQueue);
        vk.GetDeviceQueue(logicalDevice, indices.PresentFamily!.Value, 0, out presentQueue);

        if (enableValidationLayers)
        {
            _ = SilkMarshal.Free((nint)createInfo.PpEnabledLayerNames);
        }

        _ = SilkMarshal.Free((nint)createInfo.PpEnabledExtensionNames);

        return logicalDevice;
    }

    private void CreateCommandPool()
    {
        var queueFamiliyIndicies = FindQueueFamiliesInternal(physicalDevice);

        CommandPoolCreateInfo poolInfo =
            new()
            {
                SType = StructureType.CommandPoolCreateInfo,
                QueueFamilyIndex = queueFamiliyIndicies.GraphicsFamily!.Value,
                // added flag below to eliminate a validation layer error about clearing command buffer before recording
                Flags =
                    CommandPoolCreateFlags.TransientBit
                    | CommandPoolCreateFlags.ResetCommandBufferBit
            };

        var resultVulkan = this.vk.CreateCommandPool(device, poolInfo, null, out commandPool);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create command pool: {0}", resultVulkan);
        }
    }

    private CommandBuffer BeginSingleTimeCommands()
    {
        CommandBufferAllocateInfo allocateInfo =
            new()
            {
                SType = StructureType.CommandBufferAllocateInfo,
                Level = CommandBufferLevel.Primary,
                CommandPool = commandPool,
                CommandBufferCount = 1,
            };

        _ = vk.AllocateCommandBuffers(device, allocateInfo, out var commandBuffer);

        CommandBufferBeginInfo beginInfo =
            new()
            {
                SType = StructureType.CommandBufferBeginInfo,
                Flags = CommandBufferUsageFlags.OneTimeSubmitBit,
            };

        _ = vk.BeginCommandBuffer(commandBuffer, beginInfo);

        return commandBuffer;
    }

    private void EndSingleTimeCommands(CommandBuffer commandBuffer)
    {
        _ = vk.EndCommandBuffer(commandBuffer);

        SubmitInfo submitInfo =
            new()
            {
                SType = StructureType.SubmitInfo,
                CommandBufferCount = 1,
                PCommandBuffers = &commandBuffer,
            };

        _ = vk.QueueSubmit(graphicsQueue, 1, submitInfo, default);
        _ = vk.QueueWaitIdle(graphicsQueue);

        vk.FreeCommandBuffers(device, commandPool, 1, commandBuffer);
    }

    private bool checkValidationLayerSupport_OLD()
    {
        uint propCount = 0;
        vk.EnumerateInstanceLayerProperties(ref propCount, null);
        if (propCount == 0)
        {
            return false;
        }

        var ret = false;
        using var mem = GlobalMemory.Allocate((int)propCount * sizeof(LayerProperties));
        var props = (LayerProperties*)Unsafe.AsPointer(ref mem.GetPinnableReference());
        _ = vk.EnumerateInstanceLayerProperties(ref propCount, props);

        for (var i = 0; i < propCount; i++)
        {
            var layerName = GetString(props[i].LayerName);
            if (layerName == validationLayers[0])
            {
                ret = true;
            }
        }
        return ret;
    }

    internal static unsafe string GetString(byte* stringStart)
    {
        var characters = 0;
        while (stringStart[characters] != 0)
        {
            characters++;
        }

        return Encoding.UTF8.GetString(stringStart, characters);
    }

    private void PopulateDebugMessengerCreateInfo(ref DebugUtilsMessengerCreateInfoEXT createInfo)
    {
        createInfo.SType = StructureType.DebugUtilsMessengerCreateInfoExt;
        createInfo.MessageSeverity =
            DebugUtilsMessageSeverityFlagsEXT.VerboseBitExt
            | DebugUtilsMessageSeverityFlagsEXT.WarningBitExt
            | DebugUtilsMessageSeverityFlagsEXT.ErrorBitExt;
        createInfo.MessageType =
            DebugUtilsMessageTypeFlagsEXT.GeneralBitExt
            | DebugUtilsMessageTypeFlagsEXT.PerformanceBitExt
            | DebugUtilsMessageTypeFlagsEXT.ValidationBitExt;
        createInfo.PfnUserCallback = (DebugUtilsMessengerCallbackFunctionEXT)DebugCallback;
    }

    private void SetupDebugMessenger()
    {
        if (!enableValidationLayers)
        {
            return;
        }

        if (!vk.TryGetInstanceExtension(instance, out debugUtils))
        {
            return;
        }

        DebugUtilsMessengerCreateInfoEXT createInfo = new();
        PopulateDebugMessengerCreateInfo(ref createInfo);

        if (
            debugUtils!.CreateDebugUtilsMessenger(instance, in createInfo, null, out debugMessenger)
            != Result.Success
        )
        {
            throw new VulkanException("failed to set up debug messenger!");
        }
    }

    private uint DebugCallback(
        DebugUtilsMessageSeverityFlagsEXT messageSeverity,
        DebugUtilsMessageTypeFlagsEXT messageTypes,
        DebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData
    )
    {
        if (messageSeverity == DebugUtilsMessageSeverityFlagsEXT.VerboseBitExt)
        {
            return Vk.False;
        }

        var msg = Marshal.PtrToStringAnsi((nint)pCallbackData->PMessage);

        Debug.WriteLine($"{messageSeverity} | validation layer: {msg}");

        return Vk.False;
    }

    private SwapChainSupportDetails QuerySwapChainSupportInternal(PhysicalDevice physicalDevice)
    {
        var details = new SwapChainSupportDetails();

        _ = khrSurface.GetPhysicalDeviceSurfaceCapabilities(
            physicalDevice,
            surface,
            out details.Capabilities
        );

        uint formatCount = 0;
        _ = khrSurface.GetPhysicalDeviceSurfaceFormats(physicalDevice, surface, ref formatCount, null);

        if (formatCount != 0)
        {
            details.Formats = new SurfaceFormatKHR[formatCount];
            fixed (SurfaceFormatKHR* formatsPtr = details.Formats)
            {
                _ = khrSurface.GetPhysicalDeviceSurfaceFormats(
                    physicalDevice,
                    surface,
                    ref formatCount,
                    formatsPtr
                );
            }
        }
        else
        {
            details.Formats = [];
        }

        uint presentModeCount = 0;
        _ = khrSurface.GetPhysicalDeviceSurfacePresentModes(
            physicalDevice,
            surface,
            ref presentModeCount,
            null
        );

        if (presentModeCount != 0)
        {
            details.PresentModes = new PresentModeKHR[presentModeCount];
            fixed (PresentModeKHR* formatsPtr = details.PresentModes)
            {
                _ = khrSurface.GetPhysicalDeviceSurfacePresentModes(
                    physicalDevice,
                    surface,
                    ref presentModeCount,
                    formatsPtr
                );
            }
        }
        else
        {
            details.PresentModes = [];
        }

        return details;
    }

    private QueueFamilyIndices FindQueueFamiliesInternal(PhysicalDevice device)
    {
        var indices = new QueueFamilyIndices();

        uint queueFamilityCount = 0;
        vk.GetPhysicalDeviceQueueFamilyProperties(device, ref queueFamilityCount, null);

        var queueFamilies = new QueueFamilyProperties[queueFamilityCount];
        fixed (QueueFamilyProperties* queueFamiliesPtr = queueFamilies)
        {
            vk.GetPhysicalDeviceQueueFamilyProperties(
                device,
                ref queueFamilityCount,
                queueFamiliesPtr
            );
        }

        uint i = 0;
        foreach (var queueFamily in queueFamilies)
        {
            if (queueFamily.QueueFlags.HasFlag(QueueFlags.GraphicsBit))
            {
                indices.GraphicsFamily = i;
            }

            _ = khrSurface.GetPhysicalDeviceSurfaceSupport(device, i, surface, out var presentSupport);

            if (presentSupport)
            {
                indices.PresentFamily = i;
            }

            if (indices.IsComplete())
            {
                break;
            }

            i++;
        }

        return indices;
    }

    private bool IsDeviceSuitable(PhysicalDevice device)
    {
        var indices = FindQueueFamiliesInternal(device);

        var extensionsSupported = CheckDeviceExtensionsSupport(device);

        var swapChainAdequate = false;
        if (extensionsSupported)
        {
            var swapChainSupport = QuerySwapChainSupportInternal(device);
            swapChainAdequate =
                swapChainSupport.Formats.Length > 0 && swapChainSupport.PresentModes.Length > 0;
        }

        vk.GetPhysicalDeviceFeatures(device, out var supportedFeatures);

        PhysicalDeviceSynchronization2FeaturesKHR sync2Features =
            new()
            {
                SType = StructureType.PhysicalDeviceSynchronization2FeaturesKhr,
                Synchronization2 = Vk.True
            };

        PhysicalDeviceFeatures2 deviceFeatures2 =
            new() { SType = StructureType.PhysicalDeviceFeatures2, PNext = &sync2Features };

        vk.GetPhysicalDeviceFeatures2(device, &deviceFeatures2);

        return indices.IsComplete()
            && extensionsSupported
            && swapChainAdequate
            && supportedFeatures.SamplerAnisotropy
            && sync2Features.Synchronization2;
    }

    private bool CheckDeviceExtensionsSupport(PhysicalDevice device)
    {
        uint extentionsCount = 0;
        _ = vk.EnumerateDeviceExtensionProperties(device, (byte*)null, ref extentionsCount, null);

        var availableExtensions = new ExtensionProperties[extentionsCount];
        fixed (ExtensionProperties* availableExtensionsPtr = availableExtensions)
        {
            _ = vk.EnumerateDeviceExtensionProperties(
                device,
                (byte*)null,
                ref extentionsCount,
                availableExtensionsPtr
            );
        }

        var availableExtensionNames = availableExtensions
            .Select(extension => Marshal.PtrToStringAnsi((IntPtr)extension.ExtensionName))
            .ToHashSet();

        return deviceExtensions.All(availableExtensionNames.Contains);
    }

    private string[] GetRequiredExtensions()
    {
        var glfwExtensions = window!.VkSurface!.GetRequiredExtensions(out var glfwExtensionCount);
        var extensions = SilkMarshal.PtrToStringArray(
            (nint)glfwExtensions,
            (int)glfwExtensionCount
        );

        if (enableValidationLayers)
        {
            return [.. extensions, ExtDebugUtils.ExtensionName];
        }

        return extensions;
    }

    private bool CheckValidationLayerSupport()
    {
        uint layerCount = 0;
        _ = vk.EnumerateInstanceLayerProperties(ref layerCount, null);
        var availableLayers = new LayerProperties[layerCount];
        fixed (LayerProperties* availableLayersPtr = availableLayers)
        {
            _ = vk.EnumerateInstanceLayerProperties(ref layerCount, availableLayersPtr);
        }

        var availableLayerNames = availableLayers
            .Select(layer => Marshal.PtrToStringAnsi((IntPtr)layer.LayerName))
            .ToHashSet();

        return validationLayers.All(availableLayerNames.Contains);
    }

    private SampleCountFlags GetMaxUsableSampleCount()
    {
        vk.GetPhysicalDeviceProperties(physicalDevice, out var physicalDeviceProperties);

        var counts =
            physicalDeviceProperties.Limits.FramebufferColorSampleCounts
            & physicalDeviceProperties.Limits.FramebufferDepthSampleCounts;

        return counts switch
        {
            var c when (c & SampleCountFlags.Count64Bit) != 0 => SampleCountFlags.Count64Bit,
            var c when (c & SampleCountFlags.Count32Bit) != 0 => SampleCountFlags.Count32Bit,
            var c when (c & SampleCountFlags.Count16Bit) != 0 => SampleCountFlags.Count16Bit,
            var c when (c & SampleCountFlags.Count8Bit) != 0 => SampleCountFlags.Count8Bit,
            var c when (c & SampleCountFlags.Count4Bit) != 0 => SampleCountFlags.Count4Bit,
            var c when (c & SampleCountFlags.Count2Bit) != 0 => SampleCountFlags.Count2Bit,
            _ => SampleCountFlags.Count1Bit
        };
    }

    private Format FindSupportedFormat(
        IEnumerable<Format> candidates,
        ImageTiling tiling,
        FormatFeatureFlags features
    )
    {
        foreach (var format in candidates)
        {
            vk.GetPhysicalDeviceFormatProperties(physicalDevice, format, out var props);

            if (tiling == ImageTiling.Linear && (props.LinearTilingFeatures & features) == features)
            {
                return format;
            }
            else if (
                tiling == ImageTiling.Optimal
                && (props.OptimalTilingFeatures & features) == features
            )
            {
                return format;
            }
        }

        throw new VulkanException("failed to find supported format!");
    }

    /// <inheritdoc/>
    public unsafe void Dispose()
    {
        vk.DestroyCommandPool(device, commandPool, null);
        vk.DestroyDevice(device, null);
        GC.SuppressFinalize(this);
    }
}
