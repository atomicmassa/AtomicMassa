#version 460

const int uboLights = 10;

// Input vertex attributes
layout(location = 0) in vec3 position;     // Vertex position
layout(location = 1) in vec3 color;        // Vertex color
layout(location = 2) in vec3 normal;       // Vertex normal
layout(location = 3) in vec2 uv;           // Vertex UV coordinates

// Outputs to the fragment shader
layout(location = 0) out vec3 fragColor;       // Fragment color
layout(location = 1) out vec3 fragPosWorld;    // Fragment position in world space
layout(location = 2) out vec3 fragNormalWorld; // Fragment normal in world space

// Structure for point lights
struct PointLight {
	vec4 position;  // Light position (w component ignored)
	vec4 color;     // Light color and intensity (w component)
};

// Uniform buffer object containing global data
layout(set = 0, binding = 0) uniform GlobalUbo
{
	mat4 projection;                   // Projection matrix
	mat4 view;                         // View matrix
	vec4 front;                        // View direction
	vec4 ambientColor;                 // Ambient color and intensity
	PointLight pointLights[uboLights]; // Array of point lights
} ubo;

// Push constants for model and normal matrices
layout(push_constant) uniform Push 
{
	mat4 modelMatrix;     // Model transformation matrix
	mat4 normalMatrix;    // Normal transformation matrix
} push;

void main() {
    // Transform vertex position to world space
	vec4 positionWorld = push.modelMatrix * vec4(position, 1.0);

    // Compute the final vertex position in clip space
	gl_Position = ubo.projection * ubo.view * positionWorld;
	
    // Transform and normalize the vertex normal to world space
	fragNormalWorld = normalize(mat3(push.normalMatrix) * normal);

    // Pass the world position and color to the fragment shader
	fragPosWorld = positionWorld.xyz;
	fragColor = color;
}
