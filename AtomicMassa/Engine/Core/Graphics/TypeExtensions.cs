namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Provides extension methods for converting various data types to byte arrays.
/// </summary>
public static class TypeExtensions
{
    /// <summary>
    /// Converts an integer to a byte array.
    /// </summary>
    /// <param name="i">The integer to convert.</param>
    /// <returns>The byte array representation of the integer.</returns>
    public static byte[] AsBytes(this int i)
    {
        var bytes = new byte[4];
        BitConverter.GetBytes(i).CopyTo(bytes, 0);
        return bytes;
    }

    /// <summary>
    /// Converts a float to a byte array.
    /// </summary>
    /// <param name="f">The float to convert.</param>
    /// <returns>The byte array representation of the float.</returns>
    public static byte[] AsBytes(this float f)
    {
        var bytes = new byte[4];
        BitConverter.GetBytes(f).CopyTo(bytes, 0);
        return bytes;
    }

    /// <summary>
    /// Converts a <see cref="Vector4D"/> to a byte array.
    /// </summary>
    /// <param name="vec">The <see cref="Vector4D"/> to convert.</param>
    /// <returns>The byte array representation of the <see cref="Vector4D"/>.</returns>
    public static byte[] AsBytes(this Vector4D<float> vec)
    {
        uint offset = 0;
        uint fsize = 4;
        var bytes = new byte[16];
        BitConverter.GetBytes(vec.X).CopyTo(bytes, offset);
        BitConverter.GetBytes(vec.Y).CopyTo(bytes, offset += fsize);
        BitConverter.GetBytes(vec.Z).CopyTo(bytes, offset += fsize);
        BitConverter.GetBytes(vec.W).CopyTo(bytes, offset += fsize);

        return bytes;
    }

    /// <summary>
    /// Converts a <see cref="Matrix4X4"/> to a byte array.
    /// </summary>
    /// <param name="mat">The <see cref="Matrix4X4"/> to convert.</param>
    /// <returns>The byte array representation of the <see cref="Matrix4X4"/>.</returns>
    public static byte[] AsBytes(this Matrix4X4<float> mat)
    {
        uint offset = 0;
        uint fsize = 4;
        var bytes = new byte[64];
        for (var row = 0; row < 4; row++)
        {
            for (var col = 0; col < 4; col++)
            {
                BitConverter.GetBytes((float)mat[row, col]).CopyTo(bytes, offset);
                offset += fsize;
            }
        }
        return bytes;
    }

    /// <summary>
    /// Converts an array of <see cref="PointLight"/> to a byte array.
    /// </summary>
    /// <param name="pts">The array of <see cref="PointLight"/> to convert.</param>
    /// <returns>The byte array representation of the array of <see cref="PointLight"/>.</returns>
    public static byte[] AsBytes(this PointLight[] pts)
    {
        ArgumentNullException.ThrowIfNull(pts);

        uint offset = 0;
        var bytes = new byte[320];
        for (uint i = 0; i < 10; i++)
        {
            pts[i].GetAsBytes().CopyTo(bytes, offset);
            offset += 32;
        }

        return bytes;
    }
}
