namespace AtomicMassa.Engine.Core.Nodes;

/// <summary>
/// Allow to be visible in the node context menu
/// </summary>
[AttributeUsage(AttributeTargets.Class, Inherited = false)]
public sealed class NodeContextMenuAttribute : Attribute;
