namespace AtomicMassa.Engine.Core.Utils.File;

/// <summary>
/// Utility class for loading and creating 3D models.
/// </summary>
public static class ModelUtils
{
    /// <summary>
    /// Loads the content of an embedded resource as a string.
    /// </summary>
    /// <param name="path">The path to the embedded resource.</param>
    /// <param name="type">The type within the same assembly that contains the embedded resource.</param>
    /// <returns>The content of the embedded resource as a string.</returns>
    public static string LoadEmbeddedResource(string path, Type type)
    {
        ArgumentNullException.ThrowIfNull(type);

        using var s = type.Assembly.GetManifestResourceStream(path);
        if (s is null)
        {
            return string.Empty;
        }

        using var sr = new StreamReader(s);
        return sr.ReadToEnd();
    }

    /// <summary>
    /// Gets the text content of an embedded resource with the specified filename.
    /// </summary>
    /// <param name="filename">The filename of the embedded resource.</param>
    /// <returns>The text content of the embedded resource.</returns>
    public static string GetEmbeddedResourceObjText(string filename)
    {
        var assembly = Assembly.GetExecutingAssembly();
        // foreach (var item in assembly.GetManifestResourceNames()) { }
        var resourceName =
            assembly.GetManifestResourceNames().FirstOrDefault(s => s.EndsWith(filename, StringComparison.InvariantCultureIgnoreCase))
            ?? throw new FileNotFoundException(
                $"*** No obj file found with name {filename}\n*** Check that resourceName and try again!  Did you forget to set obj file to Embedded Resource/Do Not Copy?"
            );
        using var stream =
            assembly.GetManifestResourceStream(resourceName)
            ?? throw new FileNotFoundException(
                $"*** No shader file found at {resourceName}\n*** Check that resourceName and try again!  Did you forget to set glsl file to Embedded Resource/Do Not Copy?"
            );
        using var reader = new StreamReader(stream);
        var result = reader.ReadToEnd();
        return result;
    }

    /// <summary>
    /// Loads a 3D model from a file and returns it.
    /// </summary>
    /// <param name="vulkan">The Vulkan instance to use for rendering.</param>
    /// <param name="path">The path to the 3D model file.</param>
    /// <returns>The loaded 3D model.</returns>
    /// <exception cref="FileNotFoundException">Thrown when the specified file is not found.</exception>
    public static Model LoadModelFromFile(Vulkan vulkan, string path)
    {
        if (!System.IO.File.Exists(path))
        {
            throw new FileNotFoundException("Error loading model file, Can't find file at {path}");
        }

        var builder = new ModelBuilder();
        builder.LoadModel(path);

        // FIXME
        return new Model(vulkan, builder);
    }

    /// <summary>
    /// Creates a 3D cube model with 6 faces.
    /// </summary>
    /// <param name="vulkan">The Vulkan instance to use for rendering.</param>
    /// <returns>The created 3D cube model.</returns>
    public static Model CreateCubeModel6(Vulkan vulkan)
    {
        var h = .5f;
        var builder = new ModelBuilder
        {
            Vertices =
            [
                // left face (white)
                new(new(-h, -h, -h), Color3.White),
                new(new(-h, h, h), Color3.White),
                new(new(-h, -h, h), Color3.White),
                new(new(-h, h, -h), Color3.White),
                // x+ right face (red)
                new(new(h, -h, -h), Color3.Red),
                new(new(h, h, h), Color3.Red),
                new(new(h, -h, h), Color3.Red),
                new(new(h, h, -h), Color3.Red),
                // y+ top face (green, remember y axis points down)
                new(new(-h, -h, -h), Color3.Green),
                new(new(h, -h, h), Color3.Green),
                new(new(-h, -h, h), Color3.Green),
                new(new(h, -h, -h), Color3.Green),
                // bottom face (cyan)
                new(new(-h, h, -h), Color3.Cyan),
                new(new(h, h, h), Color3.Cyan),
                new(new(-h, h, h), Color3.Cyan),
                new(new(h, h, -h), Color3.Cyan),
                // z+ nose face (blue)
                new(new(-h, -h, h), Color3.Blue),
                new(new(h, h, h), Color3.Blue),
                new(new(-h, h, h), Color3.Blue),
                new(new(h, -h, h), Color3.Blue),
                // tail face (orange)
                new(new(-h, -h, -h), Color3.Orange),
                new(new(h, h, -h), Color3.Orange),
                new(new(-h, h, -h), Color3.Orange),
                new(new(h, -h, -h), Color3.Orange),
            ],
            Indices =
            [
                0,
                1,
                2,
                0,
                3,
                1,
                4,
                5,
                6,
                4,
                7,
                5,
                8,
                9,
                10,
                8,
                11,
                9,
                12,
                13,
                14,
                12,
                15,
                13,
                16,
                17,
                18,
                16,
                19,
                17,
                20,
                21,
                22,
                20,
                23,
                21
            ]
        };

        return new Model(vulkan, builder);
    }

    /// <summary>
    /// Creates a 3D cube model with 3 faces (and 3 missing).
    /// </summary>
    /// <param name="vulkan">The Vulkan instance to use for rendering.</param>
    /// <returns>The created 3D cube model.</returns>
    public static Model CreateCubeModel3(Vulkan vulkan)
    {
        var h = .5f;
        var builder = new ModelBuilder
        {
            Vertices =
            [
                // x+ right face (red)
                new(new(h, -h, -h), Color3.Red),
                new(new(h, h, h), Color3.Red),
                new(new(h, -h, h), Color3.Red),
                new(new(h, h, -h), Color3.Red),
                // y+ top face (green, remember y axis points down)
                new(new(-h, h, -h), Color3.Green),
                new(new(h, h, h), Color3.Green),
                new(new(-h, h, h), Color3.Green),
                new(new(h, h, -h), Color3.Green),
                // z+ nose face (blue)
                new(new(-h, -h, h), Color3.Blue),
                new(new(h, h, h), Color3.Blue),
                new(new(-h, h, h), Color3.Blue),
                new(new(h, -h, h), Color3.Blue),
            ],
            Indices = [0, 1, 2, 0, 3, 1, 4, 5, 6, 4, 7, 5, 8, 9, 10, 8, 11, 9]
        };

        return new Model(vulkan, builder);
    }
}
