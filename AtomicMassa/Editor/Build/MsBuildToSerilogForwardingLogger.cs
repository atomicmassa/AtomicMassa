using Microsoft.Build.Framework;
using Serilog.Events;

namespace AtomicMassa.Editor.Build;

/// <summary>
/// Custom logger for MSBuild that forwards logs to Serilog.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="MsBuildToSerilogForwardingLogger"/> class.
/// </remarks>
/// <param name="logger">The Serilog logger instance to forward the MSBuild logs to.</param>
public class MsBuildToSerilogForwardingLogger(Serilog.ILogger logger) : ILogger
{
    private readonly Serilog.ILogger logger = logger ?? throw new ArgumentNullException(nameof(logger));

    /// <summary>
    /// Gets or sets the level of verbosity at which to log.
    /// </summary>
    public LoggerVerbosity Verbosity { get; set; } = LoggerVerbosity.Normal;

    /// <summary>
    /// Gets or sets the logger parameters.
    /// </summary>
    public string Parameters { get; set; } = string.Empty;

    /// <summary>
    /// Initializes the logger with the specified event source.
    /// </summary>
    /// <param name="eventSource">The event source to attach to.</param>
    public void Initialize(IEventSource eventSource)
    {
        if (eventSource == null)
        {
            throw new ArgumentNullException(nameof(eventSource));
        }

        eventSource.ErrorRaised += (_, e) =>
            {
                if (e == null)
                {
                    return;
                }
                logger.Error("{File}({LineNumber},{ColumnNumber}): error {Code}: {Message}",
                    e.File ?? "N/A", e.LineNumber, e.ColumnNumber, e.Code, e.Message);
            };
        eventSource.WarningRaised += (_, e) =>
            {
                if (e == null)
                {
                    return;
                }
                logger.Warning("{File}({LineNumber},{ColumnNumber}): warning {Code}: {Message}",
                    e.File ?? "N/A", e.LineNumber, e.ColumnNumber, e.Code, e.Message);
            };
        eventSource.MessageRaised += (_, e) =>
            {
                if (e == null || string.IsNullOrEmpty(e.Message))
                {
                    return;
                }
                logger.Write((LogEventLevel)e.Importance, e.Message);
            };
    }

    /// <summary>
    /// Performs any finalization necessary before the logger is closed.
    /// </summary>
    public void Shutdown() { }
}
