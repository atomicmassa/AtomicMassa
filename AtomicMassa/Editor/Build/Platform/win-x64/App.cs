﻿namespace AtomicMassa;

/// <summary>
/// Main loop app
/// </summary>
public class App : IDisposable
{
    private const string assetFolder = @"Assets/";

    private readonly ILogger logger;

    private readonly WindowManager windowManager;

    private readonly RendererManager rendererManager;

    private readonly AssetDatabase assetDatabase;

    private readonly InputManager inputManager;

    private readonly Vulkan vulkan;

    private Node? nodeMain;

    // private ImGuiController imGuiController = null!;

    // private Usercode.IMGuiApp _imGui = null!;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rendererManager"></param>
    /// <param name="windowManager"></param>
    /// <param name="assetDatabase"></param>
    /// <param name="inputManager"></param>
    /// <param name="vulkan"></param>
    /// <param name="logger"></param>
    /// <exception cref="Exception"></exception>
    public App(
        RendererManager rendererManager,
        WindowManager windowManager,
        AssetDatabase assetDatabase,
        InputManager inputManager,
        Vulkan vulkan,
        ILogger logger
    )
    {
        this.logger = logger;

        this.windowManager = windowManager;
        if (windowManager is null)
        {
            throw new Exception("Error on creating window manager");
        }

        this.vulkan = vulkan;

        this.rendererManager = rendererManager;

        this.inputManager = inputManager;

        this.assetDatabase = assetDatabase;
    }
    /// <summary>
    /// Initialize all needed services and start the main loop
    /// </summary>
    public void Initialize()
    {
        windowManager.Initialize();
        windowManager.Window.Render += Render;
        windowManager.Window.FramebufferResize += Resize;
        rendererManager.Initialize();

        CreateAssetDatabase();

        // FIXME: get the first scene using AppSettings
        var nodeMainFilePath = assetFolder + "scene-01.asset.json";
        // var cubeBaseTemp = new Node();
        // FIXME: do not generate a new scene
        // DemoCreateScene(nodeMainFilePath);
        LoadScene(nodeMainFilePath);
        logger.Lap("startup", "objects loaded");

        inputManager.OnKeyPressed += OnKeyPressed;

        // InitializeImGui();
        // _rendererManager.OnRender += _imGuiController.Render;
        // _imGui.FirstAppGuiInit();

        Resize(windowManager.Window.FramebufferSize);

        inputManager.Initalize();

        windowManager.Window.Run();

        vulkan.Vk.DeviceWaitIdle(vulkan.Device.VkDevice);
    }

    // private void InitializeImGui()
    // {
    //     if (_nodeMain is null)
    //     {
    //         return;
    //     }
    //     _imGui = new(_nodeMain);
    //     _imGuiController = new ImGuiController(
    //         _vulkan.Vk,
    //         _windowManager.Window,
    //         _inputManager.Input,
    //         _vulkan.Device.VkPhysicalDevice,
    //         _vulkan.Device.GraphicsFamilyIndex,
    //         SwapChain.maxframesinflight,
    //         _rendererManager.Renderer.SwapChainImageFormat,
    //         _rendererManager.Renderer.SwapChainDepthFormat,
    //         _vulkan.Device.MsaaSamples
    //     );
    //     _imGuiController.Setup(_rendererManager.Renderer.SwapChain);
    //     _logger.Lap("startup", "ImGUI loaded");
    // }

    private void CreateAssetDatabase()
    {
        assetDatabase.BuildDatabase(assetFolder);
        assetDatabase.InitializeWatcher(assetFolder);
        assetDatabase.SaveDatabase("assetDatabase.json");
    }

    private void LoadScene(string nodeMainFilePath)
    {
        nodeMain = Node.Load(nodeMainFilePath, vulkan);
        nodeMain.Initialize(null, vulkan);
    }

    // FIXME: remove this method because it is just for testing
    // private void DemoCreateScene(string nodeMainFilePath)
    // {
    //     var nodeMainTemp = Usercode.DataTest.LoadNode();
    //     nodeMainTemp.Initialize(null, _vulkan);
    //     {
    //         var jsonString = JsonSerializer.Serialize(nodeMainTemp, AmObject.JsonOptions);
    //         File.WriteAllText(nodeMainFilePath, jsonString);
    //     }
    // }

    // FIXME: does it do anything really?
    private void OnKeyPressed(Key key)
    {
        if (key == Key.Escape)
        {
            windowManager.Window.Close();
        }
    }

    private void Render(double deltaTime)
    {
        if (nodeMain is null)
        {
            return;
        }

        foreach (var node in Node.GetChildren(nodeMain))
        {
            node.OnUpdate((float)deltaTime);
            foreach (var component in node.Components)
            {
                component.OnUpdate((float)deltaTime);
            }
        }

        // imGuiController.Update((float)deltaTime);

        // _imGui.GuiUpdate();

        foreach (var camera in Node.GetChildren<CameraNode>(nodeMain))
        {
            rendererManager.Render(deltaTime, camera, nodeMain);
        }
    }

    private void Resize(Vector2D<int> newSize)
    {
        if (nodeMain is null)
            return;

        foreach (var camera in Node.GetChildren<CameraNode>(nodeMain))
        {
            camera.Resize((uint)newSize.X, (uint)newSize.Y);
        }
    }

    /// <summary>
    /// Destroy data and free memory
    /// </summary>
    public void Dispose()
    {
        windowManager.Dispose();
        rendererManager.Dispose();
        inputManager.Dispose();
        // imGuiController.Dispose();
        vulkan.Device.Dispose();

        GC.SuppressFinalize(this);
    }
}
