namespace AtomicMassa.Editor.Studio.Panels.AssetBrowser;

/// <summary>
/// Content panel
/// </summary>
[Panel("Asset Browser")]
internal sealed partial class AssetBrowser : UserControl
{
    public AssetBrowser()
    {
        var viewModel = App.Current.Services.GetRequiredService<AssetBrowserViewModel>();

        DataContext = viewModel;

        InitializeComponent();
    }
}
