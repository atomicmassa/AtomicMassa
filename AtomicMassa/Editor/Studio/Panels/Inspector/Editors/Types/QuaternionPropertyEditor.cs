using Avalonia.Media;
using Silk.NET.Maths;

namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors.Types;

/// <summary>
/// Custom editor for Vector3 properties.
/// </summary>
[CustomEditor(typeof(Quaternion<float>))]
public class QuaternionPropertyEditor : UserControl
{
    private readonly TextBox xBox;
    private readonly TextBox yBox;
    private readonly TextBox zBox;
    private readonly MemberInfo memberInfo;

    /// <summary>
    /// Content of the panel
    /// </summary>
    protected StackPanel Panel { get; init; } = new();

    /// <inheritdoc/>
    public QuaternionPropertyEditor(MemberInfo memberInfo, object targetObject)
    {
        this.memberInfo = memberInfo;

        // Horizontal StackPanel for dimension TextBoxes and Labels
        var dimensionsPanel = new StackPanel { Orientation = Orientation.Horizontal, Spacing = 5, };

        var member = (Quaternion<float>)memberInfo.GetValue(targetObject)!;
        var euler = member.ToEulerDegrees();

        // X Dimension
        xBox = CreateDimensionTextBox(
            euler.X,
            VectorValueColors.X,
            "X"
        );
        xBox.KeyUp += (sender, e) => UpdateVectorComponent(sender, e, targetObject, Vector3D<float>.UnitX);

        // Y Dimension
        yBox = CreateDimensionTextBox(
            euler.Y,
            VectorValueColors.Y,
            "Y"
        );
        yBox.KeyUp += (sender, e) => UpdateVectorComponent(sender, e, targetObject, Vector3D<float>.UnitY);

        // Z Dimension
        zBox = CreateDimensionTextBox(
            euler.Z,
            VectorValueColors.Z,
            "Z"
        );
        zBox.KeyUp += (sender, e) => UpdateVectorComponent(sender, e, targetObject, Vector3D<float>.UnitZ);

        dimensionsPanel.Children.Add(CreateDimensionLabel("x", VectorValueColors.X));
        dimensionsPanel.Children.Add(xBox);
        dimensionsPanel.Children.Add(CreateDimensionLabel("y", VectorValueColors.Y));
        dimensionsPanel.Children.Add(yBox);
        dimensionsPanel.Children.Add(CreateDimensionLabel("z", VectorValueColors.Z));
        dimensionsPanel.Children.Add(zBox);

        var fieldPanel = new StackPanel
        {
            Orientation = Orientation.Horizontal,
            Margin = new Thickness(0, 5, 0, 5)
        };
        fieldPanel.Children.Add(
            new TextBlock
            {
                Text = (memberInfo?.Name ?? "").ToReadableName(),
                Width = 100
            }
        );
        fieldPanel.Children.Add(dimensionsPanel);
        Panel = new StackPanel
        {
            Orientation = Orientation.Vertical,
            Margin = new Thickness(0, 5, 0, 5)
        };
        Panel.Children.Add(fieldPanel);
        Content = Panel;
    }

    private static TextBox CreateDimensionTextBox(float value, Color color, string tooltip)
    {
        var textBox = new TextBox
        {
            Text = value.ToString(CultureInfo.InvariantCulture),
            BorderBrush = new SolidColorBrush(color),
        };
        ToolTip.SetTip(textBox, tooltip);
        return textBox;
    }

    private static TextBlock CreateDimensionLabel(string label, Color color)
    {
        return new TextBlock
        {
            Text = label,
            Width = 15,
            VerticalAlignment = VerticalAlignment.Center,
            TextAlignment = TextAlignment.Center,
            Background = new SolidColorBrush(color),
        };
    }

    private void UpdateVectorComponent(
        object? sender,
        KeyEventArgs _,
        object targetObject,
        Vector3D<float> unitVector
    )
    {
        if (sender == null)
        {
            return;
        }

        if (
            float.TryParse(
                ((TextBox)sender).Text,
                NumberStyles.Any,
                CultureInfo.InvariantCulture,
                out var component
            )
        )
        {
            var member = (Quaternion<float>)memberInfo.GetValue(targetObject)!;
            var vector3 = member.ToEulerDegrees();

            vector3 = new Vector3D<float>(
                xBox == sender ? component : vector3.X,
                yBox == sender ? component : vector3.Y,
                zBox == sender ? component : vector3.Z
            );
            memberInfo.SetValue(targetObject, vector3.ToQuaternion());
        }
    }
}
