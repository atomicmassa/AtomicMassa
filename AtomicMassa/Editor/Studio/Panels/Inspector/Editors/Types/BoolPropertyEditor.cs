namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors.Types;

/// <summary>
/// Property editor for bools
/// </summary>
[CustomEditor(editorType: typeof(bool))]
public class BoolPropertyEditor : SimpleTextFieldPropertyEditor
{
    private readonly CheckBox checkBox;

    /// <inheritdoc/>
    public BoolPropertyEditor(MemberInfo memberInfo, object targetObject)
        : base(memberInfo)
    {
        checkBox = new CheckBox { IsChecked = (bool)(memberInfo.GetValue(targetObject) ?? false) };

        checkBox.IsCheckedChanged += (_, _) =>
        {
            memberInfo.SetValue(targetObject, checkBox.IsChecked);
        };

        Panel.Children.Add(checkBox);
    }
}
