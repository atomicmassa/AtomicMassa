using System.ComponentModel;
using AtomicMassa.Editor.Build.Settings;
using AtomicMassa.Engine.Core.Asset;
using AtomicMassa.Engine.Core.Asset.Database;
using ReactiveUI;

namespace AtomicMassa.Editor.Studio.Panels.Inspector;

[InternalService]
internal sealed partial class InspectorViewModel : ObservableObject
{
    private readonly ISelectedObjectProvider<AmObject> selectedObjectProvider;
    private readonly ISelectedObjectProvider<Asset> selectedAssetProvider;
    private readonly IAppSettings appSettings;

    public new event PropertyChangedEventHandler? PropertyChanged;

    private bool preventDuplicateCall;

    private void RaisePropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    private object? selectedAsset;

    private object? selectedObject;
    public object? SelectedObject
    {
        get => selectedObject;
        set
        {
            if (selectedObject != value)
            {
                selectedObject = value;
                RaisePropertyChanged(nameof(SelectedObject));
                UpdatePropertyItems();
            }
        }
    }

    [ObservableProperty]
    private IEnumerable<Control> propertyItems = Enumerable.Empty<Control>();

    public InspectorViewModel(
        IAppSettings appSettings,
        AssetDatabase assetDatabase,
        ISelectedObjectProvider<AmObject> selectedObjectProvider,
        ISelectedObjectProvider<Asset> selectedAssetProvider)
    {
        this.selectedObjectProvider = selectedObjectProvider;
        this.selectedAssetProvider = selectedAssetProvider;
        this.appSettings = appSettings;

        _ = selectedObjectProvider.SelectedObjectChanged
            .ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(args =>
            {
                SelectedObject = args.EventArgs.New ?? selectedAsset;
            });

        _ = selectedAssetProvider.SelectedObjectChanged
            .ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(args =>
            {
                if (args.EventArgs.New is DataAsset dataAsset)
                {
                    selectedAsset = dataAsset.GetContent(appSettings.ProjectAbsoluteDir);
                }
                else
                {
                    selectedAsset = args.EventArgs.New;
                }
                SelectedObject = selectedAsset;
            });
    }

    private async void UpdatePropertyItems()
    {
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            if (SelectedObject is not null)
            {
                if (!preventDuplicateCall)
                {
                    preventDuplicateCall = true;
                    PropertyItems = PropertyEditorFactory.CreateEditors(SelectedObject);
                    preventDuplicateCall = false;
                }
            }
            else
            {
                PropertyItems = Enumerable.Empty<Control>();
            }
        });
    }
}
