using AtomicMassa.Editor.Studio.Panels.Inspector.Editors.Types;

namespace AtomicMassa.Editor.Studio.Panels.Inspector;

/// <summary>
/// Create all property editors, recursively, for a given object
/// </summary>
public static class PropertyEditorFactory
{
    /// <summary>
    /// Scan the object for all public properties and fields and create a property editor for each
    /// </summary>
    /// <param name="targetObject"></param>
    /// <returns></returns>
    public static IEnumerable<Control> CreateEditors(object targetObject)
    {
        ArgumentNullException.ThrowIfNull(targetObject);
        var members = targetObject
            .GetType()
            .GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
            .Where(member => member.MemberType is MemberTypes.Field or MemberTypes.Property);

        var memberPanels = new List<Control>();

        foreach (var memberInfo in members)
        {
            if (ShouldDisplay(memberInfo))
            {
                var editor = CreateEditor(memberInfo, targetObject);

                if (editor != null)
                {
                    memberPanels.Add(editor);
                }
            }
        }
        return memberPanels;
    }

    private static bool ShouldDisplay(MemberInfo member)
    {
        var isPublic = member switch
        {
            PropertyInfo propertyInfo => (propertyInfo.GetMethod?.IsPublic ?? false) && propertyInfo.CanWrite && propertyInfo.CanRead,
            FieldInfo fieldInfo => fieldInfo.IsPublic,
            _ => false
        };

        return (isPublic && member.GetCustomAttribute<HideInEditorAttribute>() == null)
            || member.GetCustomAttribute<ShowInEditorAttribute>() != null;
    }

    /// <summary>
    /// Creates a property editor for a given member
    /// </summary>
    /// <param name="targetObject"></param>
    /// <param name="memberInfo"></param>
    /// <returns></returns>
    public static Control? CreateEditor(MemberInfo memberInfo, object targetObject)
    {
        ArgumentNullException.ThrowIfNull(memberInfo);

        var propertyEditorType = FindCustomEditorForType(memberInfo);

        try
        {
            if (propertyEditorType != null)
            {
                if (
                    propertyEditorType.IsGenericType
                    && propertyEditorType.GetGenericTypeDefinition() == typeof(List<>)
                )
                {
                    var elementType = propertyEditorType.GetGenericArguments()[0];
                    var listEditorType = typeof(ListPropertyEditor<>).MakeGenericType(elementType);
                    return Activator.CreateInstance(
                            listEditorType,
                            [memberInfo, targetObject]
                        ) as UserControl;
                }

                // var constructor = propertyEditorType.GetConstructor(new[] { typeof(MemberInfo), typeof(object) });
                // if (constructor == null)
                // {
                //     throw new InvalidOperationException($"The property editor type '{propertyEditorType}' does not have a constructor that takes a MemberInfo and an object.");
                // }

                var userControl =
                    Activator.CreateInstance(propertyEditorType, [memberInfo, targetObject])
                    as UserControl;
                return userControl;
            }
        }
        catch (Exception ex)
        {
            Log.Error(ex, "Error on showing {member} field", memberInfo.Name);
            return null;
        }

        return null;
    }

    private static Type? FindCustomEditorForType(MemberInfo member)
    {
        Type? memberType = null;
        switch (member)
        {
            case PropertyInfo propertyInfo:
                memberType = propertyInfo.PropertyType;
                break;
            case FieldInfo fieldInfo:
                memberType = fieldInfo.FieldType;
                break;
        }

        if (memberType == null)
        {
            return null;
        }

        // If it's a List<T>, return ListPropertyEditor<T>
        if (memberType.IsGenericType && memberType.GetGenericTypeDefinition() == typeof(List<>))
        {
            var elementType = memberType.GetGenericArguments()[0];
            var listEditorType = typeof(ListPropertyEditor<>).MakeGenericType(elementType);
            return listEditorType;
        }

        // Get all types with the CustomEditorAttribute
        var customEditorTypes = BuildManager.Instance.LoadedAssemblies
            .SelectMany(assembly => assembly.GetLoadableTypes())
            .Where(type => type.GetCustomAttributes<CustomEditorAttribute>(false).Any())
            .ToList();

        Type? defaultEditorType = null;

        foreach (var editorType in customEditorTypes)
        {
            var customEditorAttributes = editorType.GetCustomAttributes<CustomEditorAttribute>(
                false
            );

            foreach (var attribute in customEditorAttributes)
            {
                if (attribute.EditorType.IsAssignableFrom(memberType))
                {
                    // Check if the editor type also has the DefaultEditorAttribute
                    if (!editorType.GetCustomAttributes<DefaultEditorAttribute>(false).Any())
                    {
                        return editorType;
                    }
                    else
                    {
                        // If the found editor type is a default editor, store it as a fallback
                        defaultEditorType ??= editorType;
                    }
                }
            }
        }

        // Fallback to ClassPropertyEditor if no custom editor was found
        // if (defaultEditorType == null)
        // {
        //     if (memberType.IsValueType && !memberType.IsPrimitive)
        //     {
        //         return typeof(ClassPropertyEditor);
        //     }
        // }
        if (defaultEditorType == null)
        {
            if (memberType.IsClass || (memberType.IsValueType && !memberType.IsPrimitive))
            {
                return typeof(ClassPropertyEditor);
            }
        }

        return defaultEditorType;
    }
}
