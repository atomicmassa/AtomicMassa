using AtomicMassa.Engine.Core.Asset;
using ReactiveUI;
using System.ComponentModel;

namespace AtomicMassa.Editor.Studio.Windows.MainWindow.AssetTab;

[InternalService]
internal sealed class AssetTabViewModel : ReactiveObject, INotifyPropertyChanged
{
    public ObservableCollection<AssetWrapper?> Assets { get; } = [];

    private readonly ISelectedObjectProvider<AmObject> selectedObjectProvider;
    private readonly ISelectedObjectProvider<Node> selectedNodeProvider;
    private readonly ISelectedObjectProvider<Asset> selectedAssetProvider;

    public AssetTabViewModel(
        ISelectedObjectProvider<AmObject> selectedObjectProvider,
        ISelectedObjectProvider<Asset> selectedAssetProvider,
        ISelectedObjectProvider<Node> selectedNodeProvider
        )
    {
        this.selectedObjectProvider = selectedObjectProvider;
        this.selectedAssetProvider = selectedAssetProvider;
        this.selectedNodeProvider = selectedNodeProvider;

        _ = selectedAssetProvider.SelectedObjectChanged
            .ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(args =>
            {
                if (args.EventArgs.New != null)
                {
                    AddAsset(new(args.EventArgs.New));
                }
            });
        AddAsset(null);
    }

    public void AddAsset(AssetWrapper? asset)
    {
        if (!Assets.Contains(asset))
        {
            Assets.Add(asset);
            if (Assets.Contains(null))
            {
                RemoveAsset(null);
            }
        }
    }

    public void RemoveAsset(AssetWrapper? asset)
    {
        _ = Assets.Remove(asset);
        if (!Assets.Any())
        {
            Assets.Add(null);
        }
    }

    public void SelectAsset(AssetWrapper? asset)
    {
        selectedAssetProvider.SetSelectedObject(asset?.Asset);
    }
}
