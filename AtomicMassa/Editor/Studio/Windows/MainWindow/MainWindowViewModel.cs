using System.Threading.Tasks;
using AtomicMassa.Editor.Build.Settings;

namespace AtomicMassa.Editor.Studio.Windows.MainWindow;

[InternalService]
internal sealed partial class MainWindowViewModel : ObservableObject
{
    public event Action? OnCompilationCompleted;
    public ObservableCollection<AssetWrapper?> Assets { get; } = [];

    public string Title => appSettings?.Title ?? string.Empty;

    private readonly IAppSettings appSettings;

    [ObservableProperty]
    private bool isNotificationVisible;

    [ObservableProperty]
    private string notificationText = string.Empty;

    private readonly BuildManager buildManager;

    public MainWindowViewModel(IAppSettings appSettings, BuildManager buildManager, ILogger logger)
    {
        this.appSettings = appSettings;
        Log.Information("{AppTitle}", appSettings.Title);

        this.buildManager = buildManager;
        this.buildManager.OnCompilationSuccess += ShowNotification;
        this.buildManager.OnCompilationFailure += ShowNotification;

        CompileAndLoad();
    }

    [MenuItem("File/Quit")]
    public static void Quit(Window _)
    {
        if (Application.Current?.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktopLifetime)
        {
            Dispatcher.UIThread.InvokeAsync(() => desktopLifetime.Shutdown());
        }
    }

    [MenuItem("File/Recompile")]
    static public async void CompileAndLoad(object? _ = null)
    {
        Log.Information("**************** Compilation Stated");
        var buildManager = App.Current.Services.GetRequiredService<BuildManager>();

        await Task.Run(async () =>
        {
            try
            {
                await buildManager.CompileAndLoadAssembly(true);
            }
            catch (Exception ex)
            {
                Log.Error(exception: ex, "Compilation Failed");
            }
        });
    }

    [MenuItem("File/Export")]
    static public async void Export(object? _ = null)
    {
        Log.Information("**************** Export Stated");
        var buildManager = App.Current.Services.GetRequiredService<BuildManager>();

        await Task.Run(async () =>
        {
            try
            {
                await buildManager.Export();
            }
            catch (Exception ex)
            {
                Log.Error(exception: ex, "Export Failed");
            }
        });
    }

    private void ShowNotification(string message)
    {
        OnCompilationCompleted?.Invoke();

        NotificationText = message;
        IsNotificationVisible = true;

        Task.Delay(5000).ContinueWith(_ => IsNotificationVisible = false);
    }
}
