using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AtomicMassa.CSharp.CodeGenerator;

/// <summary>
/// Generates a C# class with the list of contributors and the compilation date.
/// It parses the CONTRIBUTORS.md file and generates a static class with the
/// list of contributors and the compilation date.
/// </summary>
/// <seealso href="AboutDialog.cs"/>
[Generator]
public partial class ContributorsGenerator : IIncrementalGenerator
{
    private static string ParseContributors(string markdownContent)
    {
        var regex = MyRegex();
        var matches = regex.Matches(markdownContent);

        var contributors = new List<string>();

        foreach (var match in matches.Cast<Match>())
        {
            var name = match.Groups["name"].Value;
            var email = match.Groups["email"].Value;

            contributors.Add($"{name} ({email})");
        }

        contributors.Sort();
        return string.Join("\n", contributors);
    }

    [GeneratedRegex("\\*\\s+(?:\\[(?<name>[^\\]]+)\\]\\((?<email>[^)]+)\\)|(?<name2>[^[\\n]+))", RegexOptions.Multiline)]
    private static partial Regex MyRegex();

    /// <inheritdoc />
    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        var contributorsFile = context.AdditionalTextsProvider
            .Where(file => Path.GetFileName(file.Path) == "CONTRIBUTORS.md");

        var contributorData = contributorsFile
            .Select((file, _) => ParseContributors(file.GetText(_)?.ToString()))
            .Combine(context.CompilationProvider
                .Select((_, _) => "2024-10-21"));

        context.RegisterSourceOutput(contributorData, (spc, data) =>
        {
            var (contributorsList, compilationDate) = data;
            var source = $@"
#define ContributorsSourceGenerated

namespace AtomicMassa.Editor.Studio;

/// <summary>
/// A static class with the list of contributors and the compilation date.
/// </summary>
public static partial class Contributors
{{
    /// <summary>
    /// The list of contributors.
    /// Names and emails are sorted alphabetically.
    /// </summary>
    public const string List = @""{contributorsList}"";

    /// <summary>
    /// The compilation date. Format: yyyy-MM-dd
    /// </summary>
    public const string CompilationDate = ""{compilationDate}"";
}}";
            spc.AddSource("Contributors", SourceText.From(source, Encoding.UTF8));
        });
    }
}
