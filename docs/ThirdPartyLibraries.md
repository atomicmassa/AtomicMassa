# Third Party Stuff that We Borrowed Forever

**AtomicMassa** is made possible by the following open source libraries and frameworks. I hope you like our choices.

It's permissive enough to make even the most libertarian among us blush.

---

[Avalonia](https://github.com/AvaloniaUI/Avalonia) ([Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)) - a cross-platform UI framework that brings modern styling and layout to AtomicMassa's development environment. Let's face it, nobody wants to work with Windows Forms anymore

[NP.Avalonia.UniDock](https://github.com/npolyak/NP.Avalonia.UniDock) ([Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)) - a docking library that allows you to easily arrange and manage your development windows. We chose this library because we're convinced that life is better with a personal assistant.

[Serilog](https://github.com/serilog/serilog) ([Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)) - a diagnostic logging library that makes it easy to log events and troubleshoot issues. We need to keep track of all the weird and wonderful things that our code gets up to.

[Silk.NET](https://github.com/dotnet/Silk.NET) ([MIT License](https://opensource.org/licenses/MIT))- a high-performance graphics and compute library that makes it easy to create beautiful visuals. We want to make sure our apps look as good as they play.

---

We would like to extend our sincerest thanks to the developers of these libraries for their hard work and dedication to creating quality open source software under permissive open source licenses. And, let's be honest, we're glad we didn't have to spend any money on them.

We'll be the first to admit it: we're not perfect. It's entirely possible that we've missed a library or framework that's crucial our functionality. If that's the case, please let us know! We won't hold it against you (too much).

Without their contributions, **AtomicMassa** would not be possible.

---

And if you're a developer who's eager to [join the AtomicMassa family](CONTRIBUTING.md), we're thrilled to have you! We're always on the lookout for talented individuals who can help us take it to the next level. Whether you're a coding wizard or a documentation diva, there's a place for you in our community. So don't be shy - drop by our Discord server or Matrix chat room and say hello. We can't wait to work with you!
